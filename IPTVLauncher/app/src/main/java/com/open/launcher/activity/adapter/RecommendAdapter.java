package com.open.launcher.activity.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.open.launcher.bean.RecommendMode;
import com.open.launcher.utils.Consts;

import java.util.ArrayList;

/**
 * @author snail
 * @date 2018/5/12
 * @description
 */
public class RecommendAdapter extends PagerAdapter {

    private ArrayList<RecommendMode> data;

    public RecommendAdapter(ArrayList<RecommendMode> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(container.getContext());
        RecommendMode mode = data.get(position);
        Glide.with(container.getContext()).load(Consts.ROOT_ADDR + mode.getPoster_url()).into(imageView);
        container.addView(imageView);

        return imageView;
    }
    @Override
    public float getPageWidth(int position) {
        return 0.25f;
    }
}
