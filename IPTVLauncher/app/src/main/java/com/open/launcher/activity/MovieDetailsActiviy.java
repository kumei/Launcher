package com.open.launcher.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.open.launcher.R;
import com.open.launcher.player.UpVideoView;
import com.zhy.autolayout.AutoRelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 播放详情页面
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class MovieDetailsActiviy extends BaseActivity {

    /**
     * 播放布局
     */
    @BindView(R.id.play_content)
    UpVideoView playContent;
    @BindView(R.id.player_view)
    View playerView;

    /**
     * 播放
     */
    @BindView(R.id.lay_play_fn)
    AutoRelativeLayout layPlayFn;

    /**
     * 付费
     */
    @BindView(R.id.lay_money)
    AutoRelativeLayout layMoney;

    /**
     * 电影标题
     */
    @BindView(R.id.mv_title)
    TextView mvTitle;

    /**
     * 电影导演
     */
    @BindView(R.id.mv_director)
    TextView mvDirector;

    /**
     * 电影主演
     */
    @BindView(R.id.mv_main_actor)
    TextView mvMainActor;

    /**
     * 电影类型
     */
    @BindView(R.id.mv_type)
    TextView mvType;

    /**
     * 电影时长
     */
    @BindView(R.id.mv_all_time)
    TextView mvAllTime;

    /**
     * 电影简介
     */
    @BindView(R.id.mv_synopsis)
    TextView mvSynopsis;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actvity_movie_details);
        ButterKnife.bind(this);
        layPlayFn.requestFocus();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        initVideoInfo();
    }

    private void initVideoInfo() {
    }

    @OnClick({R.id.play_content, R.id.lay_play_fn, R.id.lay_money})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.play_content:

                break;
            case R.id.lay_play_fn: // 播放
                startActivity(new Intent(this, MediaPlayActivity.class));
                break;
            case R.id.lay_money:

                break;
        }
    }

    @OnClick({R.id.play_content, R.id.player_view, R.id.lay_play_content})
    public void onPlayerViewClicked(View view) {
        switch (view.getId()) {
            case R.id.play_content:
            case R.id.player_view:
            case R.id.lay_play_content:
                startActivity(new Intent(this, MediaPlayActivity.class));
                break;
        }
    }
}
