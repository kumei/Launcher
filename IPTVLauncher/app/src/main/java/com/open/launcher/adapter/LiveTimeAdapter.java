package com.open.launcher.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.open.launcher.R;
import com.open.launcher.bean.EpgLiveMode;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

/**
 * Created by hailongqiu on 2018/5/13.
 */

public class LiveTimeAdapter extends RecyclerView.Adapter<LiveTimeAdapter.ViewHolder> {

    List<EpgLiveMode> mEpgList;

    public void setDatas(List<EpgLiveMode> epgList) {
        this.mEpgList = epgList;
        notifyDataSetChanged();
    }

    public List<EpgLiveMode> getDatas() {
        return mEpgList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_live_time_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 130);
        view.setLayoutParams(lp);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        AutoUtils.auto(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.timeTv.setText(mEpgList.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return null != mEpgList ? mEpgList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView timeTv;

        public ViewHolder(View view) {
            super(view);
            timeTv = view.findViewById(R.id.time_tv);

        }
    }

}
