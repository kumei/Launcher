package com.open.launcher.bean;

import java.util.List;

/**
 * 直播预告信息.
 */
public class EpgLiveMode {

    String time;
    List<EpgInfo> item; // 预告的时间段内容.

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<EpgInfo> getItem() {
        return item;
    }

    public void setItem(List<EpgInfo> item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "EpgLiveMode{" +
                "time='" + time + '\'' +
                ", item=" + item +
                '}';
    }

    public class EpgInfo {
        String time;
        String proname;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getProname() {
            return proname;
        }

        public void setProname(String proname) {
            this.proname = proname;
        }

        @Override
        public String toString() {
            return "EpgInfo{" +
                    "time='" + time + '\'' +
                    ", proname='" + proname + '\'' +
                    '}';
        }

    }

}
