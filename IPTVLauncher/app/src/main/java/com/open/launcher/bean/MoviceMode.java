package com.open.launcher.bean;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 影视数据
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class MoviceMode implements Serializable {

    int id;
    String name = "未知";
    String spell_name;
    String introduction; // 描述
    String director; // 导演
    String actor; //演员
    String released_time;
    String area;
    String poster_url;
    String sort;
    int count;
    String type;
    List<EpisodesMode> episodes;

    public List<EpisodesMode> getEpisodes() {
        return episodes;
    }

    @Override
    public String toString() {
        return "MoviceMode{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", spell_name='" + spell_name + '\'' +
                ", introduction='" + introduction + '\'' +
                ", director='" + director + '\'' +
                ", actor='" + actor + '\'' +
                ", released_time='" + released_time + '\'' +
                ", area='" + area + '\'' +
                ", poster_url='" + poster_url + '\'' +
                ", sort='" + sort + '\'' +
                ", count=" + count +
                ", type='" + type + '\'' +
                ", episodes=" + episodes +
                '}';
    }

    public void setEpisodes(List<EpisodesMode> episodes) {
        this.episodes = episodes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpell_name() {
        return spell_name;
    }

    public void setSpell_name(String spell_name) {
        this.spell_name = spell_name;
    }

    public String getIntroduction() {
        return !TextUtils.isEmpty(introduction) ? introduction : "未知";
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getDirector() {
        return !TextUtils.isEmpty(director) ? director : "未知";
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return !TextUtils.isEmpty(actor) ? actor : "未知";
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getReleased_time() {
        return released_time;
    }

    public void setReleased_time(String released_time) {
        this.released_time = released_time;
    }

    public String getArea() {
        return !TextUtils.isEmpty(area) ? area : "未知";
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPoster_url() {
        return poster_url;
    }

    public void setPoster_url(String poster_url) {
        this.poster_url = poster_url;
    }

    public String getSort() {
        return !TextUtils.isEmpty(sort) ? sort : "未知";
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getType() {
        return !TextUtils.isEmpty(type) ? type: "未知";
    }

    public void setType(String type) {
        this.type = type;
    }

}
