package com.open.launcher.view;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;

import com.open.launcher.R;

/**
 * Created by hailongqiu on 2018/5/1.
 */

public class TextLoopWindow {

    Context mContext;
    WindowManager mWindowManager = null;
    View mView = null;
    ScrollTextView mScrollTextView;

    public TextLoopWindow(Context context) {
        try { // todo android 6.0 申请权限
            mContext = context.getApplicationContext();
            // 获取WindowManager
            mWindowManager = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            mView = LayoutInflater.from(context).inflate(R.layout.popupwindow,
                    null);
            //
            mScrollTextView = mView.findViewById(R.id.scroll_tv);
            //
            final WindowManager.LayoutParams params = new WindowManager.LayoutParams();
            // 类型
            params.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
            // WindowManager.LayoutParams.TYPE_SYSTEM_ALERT
            // 设置flag
            int flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;
            //FLAG_ALT_FOCUSABLE_IM;
            // | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            // 如果设置了WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE，弹出的View收不到Back键的事件
            params.flags = flags;
            // 不设置这个弹出框的透明遮罩显示为黑色
            params.format = PixelFormat.TRANSLUCENT;
            // FLAG_NOT_TOUCH_MODAL不阻塞事件传递到后面的窗口
            // 设置 FLAG_NOT_FOCUSABLE 悬浮窗口较小时，后面的应用图标由不可长按变为可长按
            // 不设置这个flag的话，home页的划屏会有问题
            params.width = WindowManager.LayoutParams.MATCH_PARENT;
            params.height = WindowManager.LayoutParams.MATCH_PARENT;
            params.gravity = Gravity.CENTER;
            mWindowManager.addView(mView, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLoopText(String text) {
        mScrollTextView.setText(text, 2);
        mScrollTextView.starScroll();
    }

    public void stopLoopText() {
        mScrollTextView.stopScroll();
    }

    public void hidePopupWindow() {
        if (null != mView) {
            mScrollTextView.stopScroll();
            mWindowManager.removeView(mView);
        }
    }

}
