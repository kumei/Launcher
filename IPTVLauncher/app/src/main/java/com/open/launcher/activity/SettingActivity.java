package com.open.launcher.activity;

import android.os.Bundle;

import com.open.launcher.R;
import com.open.launcher.fragment.LoginFragment;
import com.open.launcher.fragment.SettingsFragment;

import butterknife.ButterKnife;

/**
 * 系统设置 主界面
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class SettingActivity extends BaseActivity {

    private static final String TAG_SETTINGS =  "SETTINGS";
//    @BindView(R.id.system_settings)
//    public LinearLayout systemSettings;

    private LoginFragment mLoginFragment = null;
    private SettingsFragment mSettingsFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        mLoginFragment = new LoginFragment();
        mSettingsFragment = new SettingsFragment();
        initView();
    }

    private void initView() {
        if(!mLoginFragment.isAdded()){
            getSupportFragmentManager().beginTransaction().replace(R.id.contentlay, mLoginFragment).commitAllowingStateLoss();
//            getFragmentManager().beginTransaction().replace(R.id.contentlay, mLoginFragment).commitAllowingStateLoss();
        }
    }

//    @OnClick(R.id.system_settings)
//    public void onViewClicked() {
//        if(!mSettingsFragment.isAdded()){
//            getFragmentManager().beginTransaction().replace(R.id.contentlay, mSettingsFragment).addToBackStack(TAG_SETTINGS).commit();
//        }
//    }

    public void showSettingFragment() {
        if(!mSettingsFragment.isAdded()){
            getSupportFragmentManager().beginTransaction().replace(R.id.contentlay, mSettingsFragment).commit();
//            getFragmentManager().beginTransaction().replace(R.id.contentlay, mSettingsFragment).commit(); //.addToBackStack(TAG_SETTINGS).commit();
        }
    }

}
