package com.open.launcher.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.open.launcher.R;
import com.open.widget.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnFocusChange;
import butterknife.Unbinder;

/**
 * Created by ghq on 2018/4/6.
 */

public class RoomNoFragment extends BaseFragment {

    @BindView(R.id.et_room_no)
    EditText etRoomNo;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings_room_no, null);
        unbinder = ButterKnife.bind(this, view);
        etRoomNo.requestFocus();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnFocusChange(R.id.et_room_no)
    public void onViewFocusChange(View view, boolean isfocus){
        ViewUtils.scaleView(view, isfocus);
    }
}
