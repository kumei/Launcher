package com.open.launcher.activity;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.open.launcher.R;
import com.open.launcher.adapter.LiveEpgAdapter;
import com.open.launcher.adapter.LiveTimeAdapter;
import com.open.launcher.bean.EpgLiveMode;
import com.open.launcher.bean.LiveInfoMode;
import com.open.launcher.bean.ResultBean;
import com.open.launcher.utils.Consts;
import com.open.launcher.view.KeySplitDecoration;
import com.open.leanback.widget.OnChildSelectedListener;
import com.open.leanback.widget.VerticalGridView;
import com.open.widget.utils.ViewUtils;
import com.orhanobut.logger.Logger;
import com.tsy.sdk.myokhttp.MyOkHttp;
import com.tsy.sdk.myokhttp.response.GsonResponseHandler;
import com.tsy.sdk.myokhttp.response.JsonResponseHandler;
import com.tsy.sdk.myokhttp.response.RawResponseHandler;
import com.zhy.autolayout.utils.AutoUtils;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * 直播界面
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.04.29
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class LivePlayActivity extends MediaPlayActivity {

    MyOkHttp mMyOkHttp;
    LiveChannelAdapter mLiveChannelAdapter;
    LiveTimeAdapter mLiveTimeAdapter;
    LiveEpgAdapter mLiveEpgAdapter;
    List<LiveInfoMode> mLiveInfoModes = new ArrayList<>();
    int mPageNum = Consts.DEFUALT_PAGE;

    @BindView(R.id.live_menu_flyt)
    View live_menu_flyt;
    @BindView(R.id.one_vgridview)
    VerticalGridView oneMenuVGridView;
    @BindView(R.id.time_vgridview)
    VerticalGridView timeMenuVGridView; // 时间选择.
    @BindView(R.id.two_vgridview)
    VerticalGridView twoMenuVGridView;

    @Override
    public void initAllDatas() {
        mMyOkHttp = ((LauncherApplication)getApplicationContext()).getOkHttp();
    }

    private void testDatas() {
        for (int i = 0; i < 100; i++) {
            LiveInfoMode liveInfoMode = new LiveInfoMode();
            liveInfoMode.setChannel_url("rtmp://live.hkstv.hk.lxdns.com/live/hks");
            liveInfoMode.setChannel_name("中央" + (i+1) + "台");
            mLiveInfoModes.add(liveInfoMode);
        }
    }

    @Override
    public void initRecyclerView() {
        hGridview.setVisibility(View.GONE);
        // 直播预告信息.
        mLiveEpgAdapter = new LiveEpgAdapter();
        twoMenuVGridView.setAdapter(mLiveEpgAdapter);
        // 直播时间
        mLiveTimeAdapter = new LiveTimeAdapter();
        timeMenuVGridView.getBaseGridViewLayoutManager().setFocusOutSideAllowed(true, false);
        timeMenuVGridView.setAdapter(mLiveTimeAdapter);
        timeMenuVGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                List<EpgLiveMode> epgList = mLiveTimeAdapter.getDatas();
                // 直播預告.
                if (null != epgList && epgList.size() > 0) {
                    List<EpgLiveMode.EpgInfo> epgInfoList = epgList.get(position).getItem();
                    mLiveEpgAdapter.setDatas(epgInfoList);
                    mLiveEpgAdapter.notifyDataSetChanged();
                }
            }
        });
        // 直播频道
        mLiveChannelAdapter = new LiveChannelAdapter();
        oneMenuVGridView.addItemDecoration(new KeySplitDecoration());
        //oneMenuVGridView.getBaseGridViewLayoutManager().setFocusOutSideAllowed(true, false);
        oneMenuVGridView.setAdapter(mLiveChannelAdapter);
        oneMenuVGridView.setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                // 播放
                if (null != mLiveInfoModes && mLiveInfoModes.size() > 0) {
                    final LiveInfoMode liveInfoMode = mLiveInfoModes.get(position);
                    Logger.d("選擇直播頻道 liveInfoMode:" + liveInfoMode);
                    if (null != liveInfoMode) {
                        upVideoView.setVideoPath(liveInfoMode.getChannel_url());
                        // 设置直播时间列表.
                        List<EpgLiveMode> epgList = liveInfoMode.getEpg();
//                        mLiveTimeAdapter.setDatas(epgList);
//                        mLiveTimeAdapter.notifyDataSetChanged();
                        //
                        // 直播預告.
//                        if (null != epgList && epgList.size() > 0) {
//                            List<EpgLiveMode.EpgInfo> epgInfoList = epgList.get(position).getItem();
//                            mLiveEpgAdapter.setDatas(epgInfoList);
//                            mLiveEpgAdapter.notifyDataSetChanged();
//                        }
                    }
                }
            }
        });
        oneMenuVGridView.setOnLoadMoreListener(new VerticalGridView.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mPageNum += 1;
                refreshRequest();
            }
        });
        //
//        testDatas();
    }

    @Override
    public void refreshRequest() {
        // test 设置播放地址
//        upVideoView.setVideoPath("rtmp://live.hkstv.hk.lxdns.com/live/hks"); // test
//        upVideoView.start();
        //
        Map<String, String> params = new HashMap<>();
        params.put("mac_addr", ((LauncherApplication)getApplicationContext()).getManAddr());
//        params.put("mac_addr", "aa:bb:cc:ee:ff:dd");
        params.put("page", mPageNum + "");
        params.put("page_num", Consts.PAGE_COUNT + "");
        mMyOkHttp.post()
                .url(Consts.GET_LIVE_LIST_ADDR)
                .params(params)
                .enqueue(new GsonResponseHandler<ResultBean<List<LiveInfoMode>>>() {
                    @Override
                    public void onFailure(int statusCode, String error_msg) {
                        showToast(error_msg + "(" + statusCode + ")");
                    }

                    @Override
                    public void onSuccess(int statusCode, ResultBean<List<LiveInfoMode>> response) {
                        if (null != response) {
                            List<LiveInfoMode> liveInfoModes = response.getData();
                            Logger.d("liveInfoModes:" + response);
                            if (null != liveInfoModes && liveInfoModes.size() > 0) {
                                // 播放第一个.
                                final LiveInfoMode liveInfoMode = liveInfoModes.get(0);
                                Logger.d("選擇直播頻道 liveInfoMode:" + liveInfoMode);
                                if (null != liveInfoMode) {
                                    upVideoView.setVideoPath(liveInfoMode.getChannel_url());
                                }
                                //
                                mLiveInfoModes.addAll(liveInfoModes);
                                mLiveChannelAdapter.notifyDataSetChanged();
                                oneMenuVGridView.endMoreRefreshComplete();
//                                oneMenuVGridView.setNextFocusRightId(R.id.one_vgridview);
                                return;
                            } else {
//                                testDatas();
//                                return;
                            }
                        }
                        showToast("没有更多直播数据");
                        // 已经没有数据.
                        oneMenuVGridView.endRefreshingWithNoMoreData();
                    }
                });
    }

    @Override
    public void onDispatchKeyEvent(KeyEvent event) {
        switch (event.getKeyCode()) {
            //case KeyEvent.KEYCODE_DPAD_CENTER: // 回车按键
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (event.getAction() == KeyEvent.ACTION_DOWN && live_menu_flyt.getVisibility() == View.GONE) {
                    live_menu_flyt.setVisibility(View.VISIBLE);
                    oneMenuVGridView.requestFocusFromTouch();
//                    oneMenuVGridView.setSelectedPosition(0);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (live_menu_flyt.getVisibility() == View.GONE) {
            super.onBackPressed();
        } else {
            live_menu_flyt.setVisibility(View.GONE);
        }
    }

    public class LiveChannelAdapter extends RecyclerView.Adapter<LiveChannelAdapter.ViewHolder> {

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public LiveChannelAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = View.inflate(parent.getContext(), R.layout.item_channel_view, null);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(400, 130);
            view.setLayoutParams(lp);
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            AutoUtils.auto(view);
            LiveChannelAdapter.ViewHolder holder = new LiveChannelAdapter.ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final LiveChannelAdapter.ViewHolder holder, final int position) {
            final LiveInfoMode liveInfoMode = mLiveInfoModes.get(position);
            if (null != liveInfoMode) {
                holder.channelNameTv.setText(liveInfoMode.getChannel_name());
                holder.numTv.setText((position + 1) + "");
                //
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Logger.d("liveInfoMode:" + liveInfoMode);
                        upVideoView.setVideoPath(liveInfoMode.getChannel_url());
                        live_menu_flyt.setVisibility(View.GONE);
                        showToast("播放" + liveInfoMode.getChannel_name());
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return null != mLiveInfoModes ? mLiveInfoModes.size() : 0;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView channelNameTv;
            TextView channelInfoTv; // 信息
            TextView numTv;
            public ViewHolder(View view) {
                super(view);
                channelNameTv = view.findViewById(R.id.channel_name_tv);
                numTv = view.findViewById(R.id.num_tv);
                channelInfoTv = view.findViewById(R.id.channel_info_tv);
            }
        }

    }
}
