package com.open.launcher.bean;

/**
 * Created by hailongqiu on 2018/5/11.
 */
public class WifiApMode {
    int state; // 0 關閉， 1 開啟
    String wifi_name;
    String wifi_pswd;

    public String getWifi_name() {
        return wifi_name;
    }

    public void setWifi_name(String wifi_name) {
        this.wifi_name = wifi_name;
    }

    public String getWifi_pswd() {
        return wifi_pswd;
    }

    public void setWifi_pswd(String wifi_pswd) {
        this.wifi_pswd = wifi_pswd;
    }

    @Override
    public String toString() {
        return "WifiApMode{" +
                "wifi_namel='" + wifi_name + '\'' +
                ", wifi_pswd='" + wifi_pswd + '\'' +
                '}';
    }
}
