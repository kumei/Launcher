package com.open.launcher.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.open.launcher.R;
import com.open.launcher.activity.SettingActivity;
import com.open.widget.utils.ViewUtils;
import com.zhy.autolayout.AutoLinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.Unbinder;

/**
 * Created by ghq on 2018/4/5.
 */

public class LoginFragment extends BaseFragment {

    private static final String DEFUALT_PASS = "123456";
    private static final String DEFUALT_USER = "admin";

    @BindView(R.id.ed_account)
    EditText edAccount;
    @BindView(R.id.et_pwd)
    EditText etPwd;
    @BindView(R.id.tv_confirm)
    Button tvConfirmBtn;
    @BindView(R.id.tv_cancle)
    Button tvCancleBtn;

    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tv_confirm, R.id.tv_cancle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_confirm:
                String pass = etPwd.getText().toString();
                String user = edAccount.getText().toString();
                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass)) {
                    showToast(R.string.empty_pass_tip);
                    return;
                }

                if (DEFUALT_USER.equals(user) && DEFUALT_PASS.equals(pass)) {
                    ((SettingActivity) getActivity()).hideKeyboard(etPwd);
                    ((SettingActivity) getActivity()).showSettingFragment();
                } else {
                    showToast(R.string.pass_error_tip);
                }
                // test
//                ((SettingActivity) getActivity()).showSettingFragment();
                break;
            case R.id.tv_cancle:
                getActivity().finish();
                break;
        }
    }

    @OnFocusChange({R.id.ed_account, R.id.et_pwd, R.id.tv_confirm, R.id.tv_cancle})
    public void onViewFocusChange(View view, boolean isfocus){
        ViewUtils.scaleView(view, isfocus);
    }

}
