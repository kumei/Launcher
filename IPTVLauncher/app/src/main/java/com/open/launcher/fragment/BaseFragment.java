package com.open.launcher.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

/**
 * Created by hailongqiu on 2018/4/11.
 */

public class BaseFragment extends Fragment {

    public void showToast(int strId) {
        Toast.makeText(getActivity(), strId, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String str) {
        Toast.makeText(getActivity(), str, Toast.LENGTH_SHORT).show();
    }

}
