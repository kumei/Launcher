package com.open.launcher.utils;

/**
 * Created by hailongqiu on 2018/4/11.
 */

public class Consts {

    public static final String IP_ADDR_KEY = "ip_addr_key";
    public static final String IP_ROOM_NO_KEY = "ip_room_no_key";

    public static String ROOT_ADDR = "http://47.104.211.108";

    public static final String GET_MOVICE_TITLE = ROOT_ADDR + "/api/v1/get_sort_info"; // 获取标题栏
    public static final String GET_SERIE_INFO = ROOT_ADDR + "/api/v1/get_serie_info"; // 获取筛选的影视
    public static final String SEARCH_MOVICE = ROOT_ADDR + "/api/v1/get_search_info"; // 搜索影视

    public static final String GET_LIVE_LIST_ADDR = ROOT_ADDR + "/api/v1/get_channel_info"; // 获取直播地址

    public static final String HOME =  "/home/"; // html5欢迎页面
    public static final String LAUNCHER =  "/launcher/"; // html5桌面

    public static final String POST_RECOMMENT_MOVIE = ROOT_ADDR + "/api/v1/get_recommend_info";

    public static final int DEFUALT_PAGE = 0;
    public static final int PAGE_COUNT = 24;

}
