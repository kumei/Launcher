package com.open.launcher.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.open.launcher.R;
import com.open.launcher.activity.LivePlayActivity;
import com.open.launcher.bean.EpgLiveMode;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.List;

/**
 * 直播频道预告信息列表
 */
public class LiveEpgAdapter extends RecyclerView.Adapter<LiveEpgAdapter.ViewHolder> {

    List<EpgLiveMode.EpgInfo> mEpgInfoList;

    public LiveEpgAdapter() {
    }

    public void setDatas(List<EpgLiveMode.EpgInfo> epgInfoList) {
        this.mEpgInfoList = epgInfoList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = View.inflate(parent.getContext(), R.layout.item_live_epg_info_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 130);
        view.setLayoutParams(lp);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        AutoUtils.auto(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    }

    @Override
    public int getItemCount() {
        return null != mEpgInfoList ? mEpgInfoList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView channelNameTv;
        TextView channelInfoTv; // 信息
        TextView numTv;
        public ViewHolder(View view) {
            super(view);
            channelNameTv = view.findViewById(R.id.channel_name_tv);
            numTv = view.findViewById(R.id.num_tv);
            channelInfoTv = view.findViewById(R.id.channel_info_tv);
        }
    }

}
