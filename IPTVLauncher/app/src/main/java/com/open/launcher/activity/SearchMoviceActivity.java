package com.open.launcher.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;

import com.open.launcher.R;
import com.open.launcher.fragment.SearchFragment;

/**
 * 影视搜索
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class SearchMoviceActivity extends BaseActivity {

    private SearchFragment mSearchFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_movice);
        mSearchFragment = new SearchFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.contentlay, mSearchFragment).commit();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mSearchFragment.dispatchKeyEvent(event)) {
            return true;
        }
        return super.dispatchKeyEvent(event);
    }
}
