package com.open.launcher.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.open.launcher.R;
import com.open.widget.utils.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.Unbinder;

/**
 * Created by ghq on 2018/4/5.
 */

public class SettingsFragment extends BaseFragment {

    @BindView(R.id.servicesetting)
    RelativeLayout servicesetting;

    @BindView(R.id.netsetting)
    RelativeLayout netsetting;

    @BindView(R.id.roomnumberssetting)
    RelativeLayout roomnumberssetting;

    Unbinder unbinder;

    RoomNoFragment mRoomNoFragment;

    ServiceSettingsFragment mServiceSettingFragment;

    NetSettingsFragment mNetSettingsFragment;

    private static final String TAG_WIFI = "WIFI";
    private static final String TAG_NET = "NET";
    private static final String TAG_ROOM_NO = "ROOM_NO";



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, null);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        mNetSettingsFragment = new NetSettingsFragment();
        mRoomNoFragment = new RoomNoFragment();
        mServiceSettingFragment = new ServiceSettingsFragment();
        netsetting.requestFocusFromTouch();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.servicesetting, R.id.netsetting, R.id.roomnumberssetting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.servicesetting: // 服务端设置
                if(!mServiceSettingFragment.isAdded())
                    getFragmentManager().beginTransaction().replace(R.id.contentlay, mServiceSettingFragment).addToBackStack(TAG_WIFI).commit();
                break;
            case R.id.netsetting: // 有线设置
                if(!mNetSettingsFragment.isAdded())
                    getFragmentManager().beginTransaction().replace(R.id.contentlay, mNetSettingsFragment).addToBackStack(TAG_NET).commit();
                break;
            case R.id.roomnumberssetting: // 房间设置
                if(!mRoomNoFragment.isAdded())
                    getFragmentManager().beginTransaction().replace(R.id.contentlay, mRoomNoFragment).addToBackStack(TAG_ROOM_NO).commit();
                break;
        }
    }

//    @Override
//    public void onFocusChange(View view, boolean b) {
//        ViewUtils.scaleView(view, b);
//    }

    @OnFocusChange({R.id.servicesetting, R.id.netsetting, R.id.roomnumberssetting})
    public void onViewFocusChanged(View view ,boolean isFocus){
        ViewUtils.scaleView(view, isFocus);
    }



}


