package com.open.launcher.bean;

/**
 * 影视标题栏数据
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class MoviceTitlerMode {

    String id;
    String name;

    public MoviceTitlerMode(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
