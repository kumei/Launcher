package com.open.launcher.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.ImageView;

import com.open.launcher.R;
import com.open.launcher.bean.WebSocketGetBean;
import com.open.launcher.utils.Consts;
import com.open.library.utils.LauncherUtils;
import com.open.library.utils.PreferencesUtils;
import com.orhanobut.logger.Logger;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.open.launcher.utils.Consts.ROOT_ADDR;

/**
 * Launcher 主界面
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class MainActivity extends BaseActivity {

    String langStr = "";
    String saveUrl = "";

    @BindView(R.id.launcher_webview)
    WebView launcherWebview;
    @BindView(R.id.bg_iv)
    ImageView bgIv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        // 初始化x5 webview.
        initX5WebView();
        //
//        LauncherUtils.startApp(this, "com.open.launcher","com.open.launcher.activity.SettingActivity");
//        startActivity(new Intent(this, FiltrateMoviceActivity.class));
//        startActivity(new Intent(this, LivePlayActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateWebView(WebSocketGetBean webSocketGetBean) {
        switch (webSocketGetBean.getCode()) {
            case 0:
//                MainActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        bgIv.setVisibility(View.VISIBLE);
//                        final String roomNo = PreferencesUtils.getString(getApplicationContext(), Consts.IP_ROOM_NO_KEY);
//                        saveUrl = Consts.HOME + roomNo;
//                        Logger.d("onUpdateWebView saveUrl:" + saveUrl);
//                        launcherWebview.loadUrl(saveUrl);
//                    }
//                });
                break;
        }
    }

    private void initX5WebView() {
        // 设置
        WebSettings webSettings = launcherWebview.getSettings();
        // 本地缓存(本地缓存不过期就使用，否则使用网络)
        webSettings.setCacheMode(WebSettings.LOAD_DEFAULT);
        // 设置与Js交互的权限
        webSettings.setJavaScriptEnabled(true);
        // 设置允许JS弹窗
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        // 白屏问题.
        webSettings.setDomStorageEnabled(true);
        // JS互调接口.
        launcherWebview.addJavascriptInterface(new WebViewJsAndroid(), "android");
        //
        launcherWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView webView, String s, Bitmap bitmap) {
                super.onPageStarted(webView, s, bitmap);
            }

            @Override
            public void onPageFinished(WebView webView, String s) {
                super.onPageFinished(webView, s);
                bgIv.setVisibility(View.GONE);
            }
        });
        bgIv.setVisibility(View.VISIBLE);
        // 加载桌面首页(欢迎页面)
        final String roomNo = PreferencesUtils.getString(getApplicationContext(), Consts.IP_ROOM_NO_KEY);
        if (!TextUtils.isEmpty(roomNo)) {
            saveUrl = Consts.HOME + roomNo;
            //launcherWebview.loadUrl(saveUrl);
        } else {
            startSettingActivity();
        }
    }

    boolean isFirst = true;

    private void startSettingActivity() {
        isFirst = true;
        bgIv.setVisibility(View.VISIBLE);
        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final String roomNo = PreferencesUtils.getString(getApplicationContext(), Consts.IP_ROOM_NO_KEY);
                if (!TextUtils.isEmpty(roomNo) && isFirst) {
                    isFirst = false;
                    saveUrl = ROOT_ADDR + Consts.HOME + roomNo;
                    Logger.d("saveUrl:" + saveUrl);
                    launcherWebview.loadUrl(saveUrl);
                }
            }
        });
    }

    List<Integer> mKeyList = new ArrayList<Integer>() {
        {
            add(KeyEvent.KEYCODE_DPAD_UP);
            add(KeyEvent.KEYCODE_DPAD_UP);
            add(KeyEvent.KEYCODE_DPAD_DOWN);
            add(KeyEvent.KEYCODE_DPAD_DOWN);
            add(KeyEvent.KEYCODE_DPAD_LEFT);
            add(KeyEvent.KEYCODE_DPAD_LEFT);
            add(KeyEvent.KEYCODE_DPAD_RIGHT);
            add(KeyEvent.KEYCODE_DPAD_RIGHT);
            add(-250);
        }
    };

    // 密码解密(弹出系统设置管理员界面): 上上下下左左右右
    int mKeyIndex = 0;
    long oldTime = 0;

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP) {
            long time = System.currentTimeMillis();
            if ((time - oldTime) > 2000) {
                mKeyIndex = 0;
            }
            oldTime = time;
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    if (mKeyList.get(mKeyIndex) == KeyEvent.KEYCODE_DPAD_UP) {
                        mKeyIndex++;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_DOWN:
                    if (mKeyList.get(mKeyIndex) == KeyEvent.KEYCODE_DPAD_DOWN) {
                        mKeyIndex++;
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    if (mKeyList.get(mKeyIndex) == KeyEvent.KEYCODE_DPAD_RIGHT) {
                        mKeyIndex++;
                    }
                    if (mKeyList.get(mKeyIndex) == -250) {
                        mKeyIndex = 0;
                        isFirst = true;
                        Intent i = new Intent(getApplicationContext(), SettingActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
//                        startActivity(new Intent(this, SettingActivity.class));
                    }
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    if (mKeyList.get(mKeyIndex) == KeyEvent.KEYCODE_DPAD_LEFT) {
                        mKeyIndex++;
                    }
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public void onBackPressed() {
        if (null != launcherWebview && launcherWebview.getUrl().endsWith("/launcher")) {
            return;
        }
        launcherWebview.goBack();
    }

    /**
     * JS HTML 与 Android端 互调.
     */
    public class WebViewJsAndroid {

        /**
         * 语言设置
         */
        @JavascriptInterface
        public void onLanguageSet(final String lang) {
            switch (lang) {
                case "ENGLISH":
                    langStr = "en";
                    LauncherUtils.setLanguage(getApplicationContext(), Locale.ENGLISH);
                    break;
                case "中文":
                    langStr = "";
                    LauncherUtils.setLanguage(getApplicationContext(), Locale.CHINESE);
                    break;
            }
            Logger.d("onLanguageSet lang:" + lang);
            // http://47.104.211.108/launcher/房間號/en
            final String roomNo = PreferencesUtils.getString(getApplicationContext(), Consts.IP_ROOM_NO_KEY);
            if (!TextUtils.isEmpty(roomNo)) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        saveUrl = ROOT_ADDR + Consts.LAUNCHER + roomNo + "/" + langStr;
                        launcherWebview.loadUrl(saveUrl);
                        Logger.d("url:" + saveUrl);
                    }
                });
            } else {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startSettingActivity();
                    }
                });
            }
        }

        /**
         * 启动APP
         */
        @JavascriptInterface
        public void onStartApp(String packName, String className) {
            LauncherUtils.startApp(MainActivity.this, packName, className);
        }

        /**
         * 跳转网页
         *
         * @param url
         */
        @JavascriptInterface
        public void setUrl(final String url) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    launcherWebview.loadUrl(url);
                }
            });
        }

    }

}
