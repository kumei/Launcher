package com.open.launcher.bean;

import java.util.List;

/**
 * 直播信息数据
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.04.29
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class LiveInfoMode {

    String channel_name; // cctv3-中央电视台
    String channel_url; // igmp://239.93.25.67:5142
    String group_name; // 大厅机顶盒专用组
    String sort_id; //  1
    String sort_name; // 中央电视台
    List<EpgLiveMode> epg;

    public void setEpg(List<EpgLiveMode> epg) {
        this.epg = epg;
    }

    public List<EpgLiveMode> getEpg() {
        return this.epg;
    }

    public String getChannel_name() {
        return channel_name;
    }

    public void setChannel_name(String channel_name) {
        this.channel_name = channel_name;
    }

    public String getChannel_url() {
        return channel_url;
    }

    public void setChannel_url(String channel_url) {
        this.channel_url = channel_url;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getSort_id() {
        return sort_id;
    }

    public void setSort_id(String sort_id) {
        this.sort_id = sort_id;
    }

    public String getSort_name() {
        return sort_name;
    }

    public void setSort_name(String sort_name) {
        this.sort_name = sort_name;
    }

    @Override
    public String toString() {
        return "LiveInfoMode{" +
                "channel_name='" + channel_name + '\'' +
                ", channel_url='" + channel_url + '\'' +
                ", group_name='" + group_name + '\'' +
                ", sort_id='" + sort_id + '\'' +
                ", sort_name='" + sort_name + '\'' +
                ", epg='" + epg + '\'' +
                '}';
    }

}
