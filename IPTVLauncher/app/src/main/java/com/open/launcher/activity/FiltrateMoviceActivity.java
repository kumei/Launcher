package com.open.launcher.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.open.launcher.R;
import com.open.launcher.bean.MoviceTitlerMode;
import com.open.launcher.bean.ResultBean;
import com.open.launcher.fragment.FiltrateMoviceFragment;
import com.open.launcher.utils.Consts;
import com.open.launcher.view.SpaceItemDecoration;
import com.open.leanback.widget.BaseGridView;
import com.open.leanback.widget.HorizontalGridView;
import com.open.leanback.widget.OnChildSelectedListener;
import com.tsy.sdk.myokhttp.MyOkHttp;
import com.tsy.sdk.myokhttp.response.GsonResponseHandler;
import com.zhy.autolayout.utils.AutoUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 影视筛选界面
 *
 * @Author: hailong.qiu
 * @Maintainer: hailong.qiu
 * @Date: 2018.03.26
 * @Copyright: 2018   Inc. All rights reserved.
 */
public class FiltrateMoviceActivity extends BaseActivity {

    List<MoviceTitlerMode> mTitlerList = new ArrayList<MoviceTitlerMode>();
    List<FiltrateMoviceFragment> mFragmentList = new ArrayList<FiltrateMoviceFragment>();

    MyOkHttp mMyOkhttp;
    TitlerAdapter mTitlerAdapter;
    FragAdapter mFragAdapter;

    @BindView(R.id.title_hgridview)
    HorizontalGridView titleHgridview;
    @BindView(R.id.page_vp)
    ViewPager pageVp;

    @BindView(R.id.search_btn)
    Button searchBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtrate_movice);
        ButterKnife.bind(this);
        initAllDatas();
        initAllViews();
        refshRequest();
    }

    private void initAllDatas() {
        mMyOkhttp = ((LauncherApplication)getApplicationContext()).getOkHttp();
        //
        mTitlerList.add(new MoviceTitlerMode("", "电影"));
        mTitlerList.add(new MoviceTitlerMode("", "电视剧"));
//        mTitlerList.add(new MoviceTitlerMode("", "综艺"));
//        mTitlerList.add(new MoviceTitlerMode("", "动漫"));
//        mTitlerList.add(new MoviceTitlerMode("", "财经"));
//        mTitlerList.add(new MoviceTitlerMode("", "体育"));
    }

    private void initAllViews() {
        initTitlerRecyclerView();
        initContentViews();
    }

    private void initTitlerRecyclerView() {
//        titleHgridview.getBaseGridViewLayoutManager().setFocusOutAllowed(true, true);
//        titleHgridview.getBaseGridViewLayoutManager().setFocusOutSideAllowed(true, true);
//        titleHgridview.getBaseGridViewLayoutManager().setFocusScrollStrategy(BaseGridView.FOCUS_SCROLL_ITEM);
        mTitlerAdapter = new TitlerAdapter();
        titleHgridview.setPadding(60, 0, 0, 0);
        int top = AutoUtils.getPercentHeightSizeBigger(0);
        int right = AutoUtils.getPercentWidthSizeBigger(20);
        titleHgridview.addItemDecoration(new SpaceItemDecoration(right, top));
        AutoUtils.autoSize(titleHgridview);
        titleHgridview.setAdapter(mTitlerAdapter);
        titleHgridview.getBaseGridViewLayoutManager().setOnChildSelectedListener(new OnChildSelectedListener() {
            @Override
            public void onChildSelected(ViewGroup parent, View view, int position, long id) {
                // TODO xml 设置 android:duplicateParentState="true" selector 无效，临时这样处理.
                // 保持选中的颜色.
                TextView tv = view.findViewById(R.id.title_tv);
                tv.setTextColor(getResources().getColor(R.color.title_select_color));
                // 翻页.
                int index = (int) view.getTag();
                if (index >= 0) {
                    pageVp.setCurrentItem(index);
                }
            }
        });
        //
        titleHgridview.setOnKeyInterceptListener(new BaseGridView.OnKeyInterceptListener() {
            @Override
            public boolean onInterceptKeyEvent(KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_DPAD_UP) {
                    // BUG:避免焦点跑到搜索框去.
                    searchBtn.setFocusableInTouchMode(true);
                    searchBtn.setFocusable(true);
                    searchBtn.requestFocusFromTouch();
                }
                return false;
            }
        });
    }

    private void initContentViews() {
        mFragAdapter = new FragAdapter(getSupportFragmentManager());
        //
        searchBtn.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                searchBtn.setBackgroundResource(b ? R.drawable.trailer_btn_focused : R.drawable.trailer_btn_normal);
            }
        });
        searchBtn.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyEvent.getKeyCode() == KeyEvent.KEYCODE_DPAD_DOWN) {
                    searchBtn.setFocusable(false);
//                    searchBtn.setFocusableInTouchMode(false);
                    titleHgridview.requestFocusFromTouch();
                    return true;
                }
                return false;
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FiltrateMoviceActivity.this, SearchMoviceActivity.class));
            }
        });
        pageVp.setOffscreenPageLimit(4); // 缓存3个页面
        pageVp.setAdapter(mFragAdapter);
    }

    private void refshRequest() {
        mMyOkhttp.get().url(Consts.GET_MOVICE_TITLE).enqueue(new GsonResponseHandler<ResultBean<List<MoviceTitlerMode>>>() {
            @Override
            public void onFailure(int statusCode, String error_msg) {
            }

            @Override
            public void onSuccess(int statusCode, ResultBean<List<MoviceTitlerMode>> response) {
                mTitlerList = response.getData();
                mTitlerAdapter.notifyDataSetChanged();
                // 刷新
                if (null != mTitlerList) {
                    mFragmentList.clear();
                    for (MoviceTitlerMode titleMode : mTitlerList) {
                        mFragmentList.add(new FiltrateMoviceFragment(mMyOkhttp, titleMode));
                    }
                    mFragAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////   Adapter
    ////////////////////////////////////////////////////////////////////

    /**
     * 标题栏 Adapter.
     */
    public class TitlerAdapter extends RecyclerView.Adapter<TitlerAdapter.ViewHolder> {

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = View.inflate(parent.getContext(), R.layout.item_titler_layout, null);
            view.setDuplicateParentStateEnabled(true);
            view.setFocusable(true);
            view.setFocusableInTouchMode(true);
            AutoUtils.autoSize(view);
            ViewHolder holder = new ViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if (null != mTitlerList) {
                holder.itemView.setTag(position);
                holder.nameTv.setText(mTitlerList.get(position).getName());
                holder.itemView.setTag(position);
                holder.itemView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View view, boolean b) {
                        // TODO xml 设置 android:duplicateParentState="true" selector 无效，临时这样处理.
                        TextView tv = view.findViewById(R.id.title_tv);
                        View lineView = view.findViewById(R.id.title_line_view);
                        lineView.setBackgroundColor(getResources().getColor(b ? R.color.title_select_color : R.color.clear_color));
                        // 焦点已不再.
                        if ((int) view.getTag() != pageVp.getCurrentItem()) {
                            tv.setTextColor(getResources().getColor(b ? R.color.title_select_color : R.color.title_none_color));
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return null != mTitlerList ? mTitlerList.size() : 0;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView nameTv;

            public ViewHolder(View view) {
                super(view);
                nameTv = view.findViewById(R.id.title_tv);
            }
        }

    }

    /**
     * 内容 framgnt Adapter.
     */
    public class FragAdapter extends FragmentPagerAdapter {

        public FragAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return null != mFragmentList ? mFragmentList.size() : 0;
        }

    }

}
